import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createSwitchNavigator, createStackNavigator, createAppContainer,createBottomTabNavigator } from 'react-navigation';
import firebase from "react-native-firebase"
import splash from './src/splashscreen'
import login from './src/auth/login'
import register_donatur from './src/auth/register_donatur'
import register_yatim from './src/auth/register_yatim'
import register_peduli from './src/auth/register_peduli'

import confirmlogin from './src/auth/confirmlogin'
import sintro from './src/auth/sintro'
import subintro from './src/auth/subintro'

import home from './src/main/home'
import service from './src/main/service'
import transaksi from './src/main/transaksi'
import profile from './src/main/profile'
import tanggungan from './src/main/profile_tamtang'

import listdonation from './src/donasi/list_userdonation'
import detaildonation from './src/donasi/detail_userdonation'
import donasi from './src/donasi/donasi'
import resume from './src/donasi/resume'

import listtaaruf from './src/taaruf/list_usertaaruf'
import detailtaaruf from './src/taaruf/detail_usertaaruf'
import taaruf from './src/taaruf/taaruf'
import resume_taaruf from './src/taaruf/resume'


const Tabmenu = createBottomTabNavigator({
  home           : home,
  transaksi      : transaksi,
  profile        : profile,
},{
  navigationOptions:{ header:{ visible:false }},
    lazy:false,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
    tabBarTextFontFamily: 'quicksand',
    tabBarOptions: {
      activeTintColor: '#B57BA6',
      showIcon: true,
      showLabel: true,
    }
})

const Auth = createStackNavigator({
  intro           : sintro,
  subintro        : subintro,
  login           : login,
  register_peduli : register_peduli,
  register_yatim  : register_yatim,
  register_donatur: register_donatur,
  confirmlogin    : confirmlogin
},{headerMode     : 'none'})

const Splashscreen = createStackNavigator({
  splash  : splash
},{headerMode   : 'none'})

const Base = createStackNavigator({
  Tabmenu       : Tabmenu,
  listdonation  : listdonation,
  detaildonation: detaildonation,
  donasi        : donasi,
  resume        : resume,

  listtaaruf    : listtaaruf,
  detailtaaruf  : detailtaaruf,
  taaruf        : taaruf,
  resume_taaruf : resume_taaruf,
  tanggungan    : tanggungan
},{headerMode   : 'none'})

export default createAppContainer(createSwitchNavigator(
  {
    Splashscreen  : Splashscreen,
    Auth          : Auth,
    Base          : Base
  },
  {
    initialRouteName: 'Splashscreen',
  }
))

