import React,{Component} from 'react';
import LottieView from 'lottie-react-native'
import {Image,Text,View} from 'react-native';

export default class loading extends Component {
  render() {
    return (
      <View style={{alignItems:'center',justifyContent:'center',flex: 1}}>
        <LottieView
          source={require('./loading.json')}
          autoPlay
          loop
          style={{width:190,height:190,marginTop:2,marginRight:9,position:'absolute'}}
        />
      </View>
    );
  }
}