import React,{Component} from 'react';
import LottieView from 'lottie-react-native'
import {Image,Text,View} from 'react-native';

export default class intros extends Component {
  render() {
    return (
      <View style={{alignItems:'center',justifyContent:'center',flex: 1}}>
        <LottieView
          source={require('./intro.json')}
          autoPlay={true}
          loop={true}
          style={{width:190,height:190,marginTop:2,marginRight:9,position:'absolute'}}
        />
      </View>
    );
  }
}