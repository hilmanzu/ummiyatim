import React, {Component} from 'react';
import {TextInput,Picker,BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import store from 'react-native-simple-store';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import firebase from "react-native-firebase"
import Loading from '../component/loading'
import renderIf from '../component/renderIf'
import UUIDGenerator from 'react-native-uuid-generator';
var numeral = require('numeral');

export default class home extends Component{

  constructor(props) {
    super(props)
    this.ref = firebase.firestore().collection('transaksi')
      this.state = {
        loading     : true,
        nama        : '',
        image       : '',
        tujuan      : '',
        Donatur     :'',
        Jumlah      :'',
        rek         :'',
        data_tujuan :[],
        data_rek    :[],
      }
  }

  componentDidMount(){
    const {params} = this.props.navigation.state;
    const db = firebase.firestore()
    const tujuan = db.collection('master').doc('data').collection('status_ekonomi')
    const rek = db.collection('user').doc(params.id).collection('rekening')
    
      store.get('nama')
      .then((res) =>
        this.setState({Donatur:res})
      )

      tujuan.onSnapshot(async(doc)=>{
        var data = []
          doc.forEach((docs)=>{
            let item = data
            item.push({
              data : docs.data(),
              id   : docs.id
            })
          })
        this.setState({
          data_tujuan : data
        })
    })

      rek.onSnapshot(async(doc)=>{
        var data = []
          doc.forEach((docs)=>{
            let item = data
            item.push({
              data : docs.data(),
              id   : docs.id
            })
          })
        this.setState({
          data_rek : data
        })
    })

  }


  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    const { loading,anak,data_tujuan,data_rek } =this.state

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Donasi Ummi</Text>
        </View>
          <Image style={{width:Dimensions.get('window').width,height:200}} source={{uri : params.image}}/>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{alignItems:'center',marginVertical:20}}>

            <View>
              <Text style={styles.textpassword}>
                Jumlah
              </Text>
              <View style={styles.textinput}>
                  <TextInput placeHolder='cont : Rp. 11' keyboardType='numeric' onChangeText={(Jumlah)=> this.setState({Jumlah})}/>
              </View>

              <Text style={styles.textpassword}>
                Tujuan
              </Text>
              <View style={styles.itempekerjaan}>
                <Picker
                  selectedValue={this.state.tujuan}
                  onValueChange={(tujuan) => this.setState({tujuan})}>
                  <Picker.Item label='Pilih Tujuan Donasi' value={''} />
                  {data_tujuan.map((item, index) => {
                  return (<Picker.Item label={item.data.nama} value={item.data.nama} key={item.id}/>) 
                  })}
                </Picker>
              </View>

              <Text style={styles.textpassword}>
                Transfer Bank
              </Text>
              <View style={styles.itempekerjaan}>
                <Picker
                  selectedValue={this.state.rek}
                  onValueChange={(rek) => this.setState({rek})}>
                  <Picker.Item label='Pilih Tujuan Transfer' value={''} />
                  {data_rek.map((item, index) => {
                  return (<Picker.Item label={item.data.nama} value={item.data.nama + '/' + item.data.rek} key={item.id}/>) 
                  })}
                </Picker>
              </View>

            </View>
            <View style={{alignItems:'center',marginVertical:20}}>
              <TouchableOpacity style={styles.button} onPress={this.donasi}>
                <Text style={styles.textbutton}>Transfer</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>

      </View>
    );
  }

  donasi = () => {
    const {params} = this.props.navigation.state;

    const db = firebase.firestore()
    const user = db.collection('user').doc(params.id)

    if (this.state.Jumlah == ''){
      alert('Isi Jumlah Transfer')
    }else if (this.state.tujuan == ''){
      alert('Pilih tujuan donasi')
    }else if (this.state.rek == ''){
      alert('Pilih Rekening Tujuan')
    }else{

      store.get('id')
      .then((res) =>{
        UUIDGenerator.getRandomUUID((uuid) =>{
          this.ref.add({
            Donatur       : this.state.Donatur,
            Penerima      : params.penerima,
            id_donatur    : res,
            id_penerima   : params.id,
            id_transaksi  : uuid,
            jumlah        : parseInt(this.state.Jumlah),
            ket           : this.state.tujuan,
            nama_transaksi: 'Donasi',
            no_rekening   : this.state.rek,
            status        : 'Menunggu',
            waktu         : new Date(),
          })

          user.update({
            donasi : false
          })
                    
          .then(()=>
            this.props.navigation.navigate('resume',({Donatur:this.state.Donatur,Penerima: params.penerima,tujuan: this.state.rek,jumlah: parseInt(this.state.Jumlah)}))
          )

        })
      })
    }
  }
}
