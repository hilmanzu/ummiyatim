import React, {Component} from 'react';
import {BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import store from 'react-native-simple-store';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import firebase from "react-native-firebase"
var numeral = require('numeral');


const { width: viewport } = Dimensions.get('window')
const sliderWidth1 = wp(100)
const itemHorizontalMargin1 = wp(0.30)
const sliderWidth = viewport
const itemWidth = (sliderWidth1 + (itemHorizontalMargin1 * 2)) - 10

function wp(percentage){
  const value = (percentage * viewport) / 100
  return Math.round(value)
}

export default class home extends Component{

  constructor(props) {
    super(props)
      this.data = firebase.firestore().collection("user")
      this.state = {
        loading: true,
        data:[]
      }
  }

  componentDidMount(){
    const {params} = this.props.navigation.state;
    store.get('uid')
    .then((res)=>{
      
      this.data.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          data : data
        })
      })
    })
  }

  render() {
    const { navigate } = this.props.navigation;
    const { data } = this.state
    const {params} = this.props.navigation.state;
    const list = data.filter((item) => item.data.verifikasi === true && item.data.pendaftaran === 'yatim' && item.data.donasi === true)

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={{width:30,height:30,marginLeft:20}} source={require('../assets/logo.png')}/>
          <Text style={styles.headertext}>Ummi Donasi</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow:1,alignItems:'center'}}>
            <View style={{alignItems:'center',flex:1}}>
              <View style={{width:360,flexDirection: 'row',flexWrap: 'wrap',justifyContent: 'center'}}>
                {list.map((data)=>
                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('detaildonation',{id:data.id})} style={{width:150,height:150,marginTop:20,backgroundColor:'#FFFFFF',alignItems:'center',justifyContent:'center',paddingTop:9,paddingBottom:15,paddingHorizontal:10,marginHorizontal:8,borderRadius:5,elevation: 10,}}>
                    <Image source={{uri : data.data.image}} style={{width:90,height:90}}/>
                      <Text style={styles.textcategory}>{data.data.nama}</Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </ScrollView>
      </View>
    );
  }
}
