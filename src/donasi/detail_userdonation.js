import React, {Component} from 'react';
import {BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import store from 'react-native-simple-store';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import firebase from "react-native-firebase"
var numeral = require('numeral');
import Loading from '../component/loading'
import renderIf from '../component/renderIf'


const { width: viewport } = Dimensions.get('window')
const sliderWidth1 = wp(100)
const itemHorizontalMargin1 = wp(0.30)
const sliderWidth = viewport
const itemWidth = (sliderWidth1 + (itemHorizontalMargin1 * 2)) - 10

function wp(percentage){
  const value = (percentage * viewport) / 100
  return Math.round(value)
}

export default class home extends Component{

  constructor(props) {
    super(props)
      this.state = {
        loading : true,
        nama    : '',
        image   : '',
        anak    : []
      }
  }

  componentDidMount(){
    const {params} = this.props.navigation.state;
    const db = firebase.firestore()
    const items = db.collection("user").doc(params.id)
    const anak = db.collection("user").doc(params.id).collection("tanggungan")

      items.onSnapshot( async (doc) => {
        let data = doc.data()
        this.setState({ ...data,loading:false })
      })

      anak.onSnapshot(async(doc)=>{
        var data = []
          doc.forEach((docs)=>{
            let item = data
            item.push({
              data : docs.data(),
              id   : docs.id
            })
          })
        this.setState({
          anak : data
        })
    })

  }


  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    const { loading,anak } =this.state

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <Loading/>
            <Text style={{paddingTop:20}}> Menunggu Sebentar </Text>
          </View>
        )
      }

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Detail Ummi</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow:1}}>
            <Image style={{width:Dimensions.get('window').width,height:200}} source={{uri : this.state.image}}/>
            <Text style={{marginTop:10,marginBottom:10,fontSize:20,fontWeight:'bold',marginLeft:20}}>{this.state.nama}</Text>
            <View style={{flexDirection:'row'}}>
              <View style={{flex:1,marginLeft:20}}>
                <Text>Nama</Text>
                <Text>Tempat Lahir</Text>
                <Text>Status</Text>
                <Text>Agama</Text>
                <Text>Tanggungan</Text>
                <Text>Pekerjaan</Text>
                <Text>Kecamatan</Text>
                <Text>Minat</Text>
                <Text>Ingin Menikah</Text>
                <Text>Perekonomian</Text>
              </View>
              <View style={{flex:2}}>
                <Text>: {this.state.nama}</Text>
                <Text>: {this.state.tempat_lahir}/ {this.state.tanggal_lahir}</Text>
                <Text>: {this.state.status}</Text>
                <Text>: {this.state.agama}</Text>
                <Text>: {this.state.tanggungan}</Text>
                <Text>: {this.state.pekerjaan}</Text>
                <Text>: {this.state.kecamatan}</Text>
                <Text>: {this.state.minat}</Text>
                {renderIf(this.state.minat_rt === true ? true : false)(
                  <Text>: Berminat</Text>
                )}
                {renderIf(this.state.minat_rt !== true ? true : false)(
                  <Text>: Tidak Berminat</Text>
                )}
                <Text>: {this.state.kondisi_eko}</Text>
              </View>
            </View>
            <Text style={{marginTop:10,marginBottom:10,fontSize:20,fontWeight:'bold',marginLeft:20}}>Tanggungan</Text>
            <View style={{alignItems:'center',justifyContent:'center'}}>
              {anak.map((data)=>
                <View style={{marginVertical:10,marginHorizontal:2}}>
                  <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} >
                    <Image source={{uri: data.data.image}} style={{position:'absolute',marginLeft:10,width:120,height:100,marginTop:10}}/>
                    <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.data.nama}</Text>
                    <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                      <Text numberOfLines={1} style={{width:150,fontSize:12}}>Tanggal Lahir , {data.data.tanggal_lahir}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                      <Text style={{fontSize:12}}>Sekolah , {data.data.sekolah}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                      <Text style={{fontSize:12}}>Prestasi , {data.data.prestasi}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                      <Text style={{fontSize:12}}>Cita - cita , {data.data.cita}</Text>
                    </View>
                  </TouchableOpacity>
                </View>
                )
              }
            </View>
            <View style={{alignItems:'center',marginVertical:20}}>
              <TouchableOpacity style={styles.button} onPress={this.daftar}>
                <Text style={styles.textbutton}>Donasi</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>

      </View>
    );
  }
  daftar=()=>{
    const { params } = this.props.navigation.state;
    this.props.navigation.navigate('donasi',{image:this.state.image,id:params.id,penerima:this.state.nama})
  }
}
