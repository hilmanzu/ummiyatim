import React, {Component} from 'react';
import {Picker,Modal,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../assets/style'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import Loading from '../component/loading'
import firebase from 'react-native-firebase';
import store from 'react-native-simple-store';
import provinsi from '../component/provinsi.json'
import kabupaten from '../component/kabupaten.json'
import kecamatan from '../component/kecamatan.json'
import renderIf from '../assets/renderIf'
import LottieView from 'lottie-react-native'
import UUIDGenerator from 'react-native-uuid-generator';

export default class login extends Component{

  constructor(props) {
    super(props)
      this.ref = firebase.firestore().collection('transaksi')
      this.state = {
        loading : true,
        image   : '',
      }
  }

  componentDidMount(){
    const db = firebase.firestore()
    store.get('id')
    .then((res)=>{

      const items = db.collection("user").doc(res)
      items.onSnapshot( async (doc) => {
        let data = doc.data()
        this.setState({ ...data,loading:false})
      })

    })

  }

  render(){
    const { navigate } = this.props.navigation;
    const { loading} =this.state

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <Loading/>
          </View>
        )
      }

    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
          <Text style={styles.headertext}>  Daftar Taaruf</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false}>

          <Text style={styles.textpassword}>
            NIK
          </Text>
          <View style={styles.textinput}>
              <TextInput value= {this.state.nik} keyboardType='numeric' onChangeText={(nik)=> this.setState({nik})}/>
          </View>

          <Text style={styles.textpassword}>
            Nama Lengkap
          </Text>
          <View style={styles.textinput}>
              <TextInput value= {this.state.nama} onChangeText={(nama)=> this.setState({nama})}/>
          </View>

          <Text style={styles.textpassword}>
            Tempat Lahir
          </Text>
          <View style={styles.textinput}>
              <TextInput value= {this.state.tempat_lahir} onChangeText={(tempat_lahir)=> this.setState({tempat_lahir})}/>
          </View>

          <Text style={styles.textpassword}>
            Tanggal Lahir
          </Text>
          <View style={styles.textinput}>
              <TextInput value= {this.state.tanggal_lahir} onChangeText={(tanggal_lahir)=> this.setState({tanggal_lahir})}/>
          </View>

          <Text style={styles.textpassword}>
            Pekerjaan
          </Text>
          <View style={styles.textinput}>
              <TextInput value= {this.state.pekerjaan} onChangeText={(pekerjaan)=> this.setState({pekerjaan})}/>
          </View>

          <Text style={styles.textpassword}>
            Minat
          </Text>
          <View style={styles.textinput}>
              <TextInput value= {this.state.minat} onChangeText={(minat)=> this.setState({minat})}/>
          </View>

          <Text style={styles.textfirst}>
            Nomor HP
          </Text>
          <View style={styles.textinput}>
            <TextInput value= {this.state.phone} keyboardType='numeric' onChangeText={(phoneNumber)=> this.setState({phoneNumber})}/>
          </View>

          <View style={{alignItems:'center',marginVertical:20}}>
            <TouchableOpacity style={styles.button} disabled={this.state.button} onPress={this.taaruf}>
              <Text style={styles.textbutton}>Taaruf Sekarang</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        </View>
      </View>
    );
  }

  taaruf = () => {
    const {params} = this.props.navigation.state;
    const db = firebase.firestore()
    const user = db.collection('user').doc(params.id)

      store.get('id')
      .then((res) =>{
        UUIDGenerator.getRandomUUID((uuid) =>{
          this.ref.add({
            Donatur       : this.state.nama,
            Penerima      : params.penerima,
            id_donatur    : res,
            id_penerima   : params.id,
            id_transaksi  : uuid,
            nama_transaksi: 'Taaruf',
            status        : 'Menunggu',
            waktu         : new Date(),
            phone_taaruf  : this.state.phone
          })

          user.update({
            minat_rt : false
          })

          .then(()=>
            this.props.navigation.navigate('resume_taaruf',({Donatur:this.state.nama,Penerima: params.penerima}))
          )
      })
    })
  }

}