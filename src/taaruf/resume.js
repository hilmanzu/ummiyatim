import React, {Component} from 'react';
import {BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import store from 'react-native-simple-store';
import firebase from "react-native-firebase"
var numeral = require('numeral');
import Loading from '../component/loading'
import renderIf from '../component/renderIf'

export default class profile extends Component{

  render() {
    const {params} = this.props.navigation.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={{width:30,height:30,marginLeft:20}} source={require('../assets/logo.png')}/>
          <Text style={styles.headertext}>Ummi Taaruf</Text>
        </View>

          <Image style={{width:100,height:100,margin:20}} source={require('../assets/taaruf.png')}/>
          <Text style={{textAlign:'center',fontSize:20,fontWeight:'bold'}}>Terima Kasih</Text>
          <Text style={{textAlign:'center',fontSize:16,fontWeight:'bold'}}>Kami akan segera melakukan verifikasi untuk melanjutkan taaruf langsung</Text>
          
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{justifyContent:'center',marginVertical:20}}>
          <View style={{padding:10}}>
            <Text style={{fontSize:20,fontWeight:'bold',marginTop:10}}>Tujuan</Text>
            <Text style={{fontSize:16}}>Taaruf</Text>
          </View>
          <View style={{padding:10}}>
            <Text style={{fontSize:20,fontWeight:'bold',marginTop:10}}>Abi</Text>
            <Text style={{fontSize:16}}>{params.Donatur}</Text>
          </View>
          <View style={{padding:10}}>
            <Text style={{fontSize:20,fontWeight:'bold',marginTop:10}}>Ummi</Text>
            <Text style={{fontSize:16}}>{params.Penerima}</Text>
          </View>
          <View style={{padding:10,alignItems:'center'}}>
            <TouchableOpacity style={styles.button} onPress={(this.donasi)}>
              <Text style={styles.textbutton}>Kembali</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>
      </View>
    );
  }

  donasi=()=>{
    this.props.navigation.navigate('transaksi')
  }
  
}
