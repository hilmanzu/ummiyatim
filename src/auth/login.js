import React, {Component,Fragment} from 'react';
import {StyleSheet,Modal,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import Loading from '../component/loading'
import firebase from 'react-native-firebase';
import store from 'react-native-simple-store';
import DropdownAlert from 'react-native-dropdownalert';
import LottieView from 'lottie-react-native'

export default class login extends Component{
  constructor(props){
    super(props);
    this.ref = firebase.firestore().collection('user')
    this.state = {
      email    :'',
      nama     :'',
      hp       :'+62',
      data     :[],
      view     : false,
      color    :'rgba(247, 247, 247, 1)'
      }
    }

  componentDidMount(){
    store.get('uid')
    .then((res)=>{
      this.ref.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          data : data
        })
        console.log(data)
      })
    })
  }

  signOut = () => {
    firebase.auth().signOut();
  }

  render() {
    const { hp } = this.state;
    return (
      <Fragment>
        <View style={styles.Content}>
          <DropdownAlert ref={ref => this.dropdown = ref} />
          <LottieView
            source={require('../component/phone.json')}
            autoPlay
            loop
            style={{width:190,height:190,marginLeft:20}}
          />
          <View style={styles.card}>
            <Text style={styles.text1}>
              Silahkan masuk dengan nomor HP-mu yang terdaftar
            </Text>
            <TextInput  
              placeholder={'Phone number ... '} 
              keyboardType='numeric'
              style={styles.textinput} 
              onChangeText={(value)=> this.setState({hp: value})}
              value={hp}
            />
            <TouchableOpacity  onPress={this.signIn}>
              <View style={styles.masuk}>
                  <Text style={styles.bottonmasuk}>Masuk</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <Modal visible={this.state.view} transparent={true}>
          <Loading/>
        </Modal>
      </Fragment>
    );
  }

  signIn=()=>{
    const {data} = this.state 
    if (this.state.hp == ''){
      alert('Isi No Handphone')
    }else{

      var Id = data.find((item) => item.data.phone === this.state.hp)
      if (Id === undefined){
        Alert.alert('Oppss', 'Kamu Belum terdaftar nih ayo segera daftar')
      }else if (Id.data.phone === this.state.hp){
        firebase.auth().signInWithPhoneNumber(this.state.hp)
        .then(confirmResult =>{
          this.dropdown.alertWithType('success', 'Success', 'Kode Telah Dikirimkan')
          this.props.navigation.navigate('confirmlogin',{confirm : confirmResult})
        })
        .catch(error =>{
          this.dropdown.alertWithType('error', 'Error', 'Masuk dengan nomor Ponsel gagal, Silahkan coba lagi')
        });
      }
    }

  }
}

const styles = StyleSheet.create({
  logo: { height: 80, marginLeft: 140, marginRight: 140, width: 80 },
  text1: {
    color: '#B57BA6',
    fontSize: 18,
    fontWeight: `bold`,
    marginLeft: 32,
    marginRight: 95,
    marginTop: 25,
    width:200
  },
  text2: {
    color: `rgba(170, 170, 170, 1)`,
    fontSize: 12,
    marginLeft: 32,
    marginRight: 85,
    marginTop: 15
  },
  textinput1: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 17,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
    width: 250,elevation:5
  },
  textinput: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 17,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
    width: 250,elevation:5
  },
  text3: {
    color: `rgba(34, 25, 77, 0.44)`,
    fontSize: 12,
    marginLeft: 45,
    marginTop: 15
  },
  bottonmasuk: { color: `rgba(255, 255, 255, 1)`, fontWeight: `bold` },
  masuk: {
    alignItems: `center`,
    backgroundColor: '#B57BA6',
    borderRadius: 30,
    height: 37,
    justifyContent: `center`,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 15,
    width: 250,elevation:5
  },
  text4: { color: '#B57BA6', textAlign: `center` },
  buttondaftar: {
    alignItems: `center`,
    justifyContent: `center`,
    marginLeft: 90,
    marginRight: 90,
    marginTop: 23
  },
  card: {
    backgroundColor: `rgba(247, 247, 247, 1)`,
    borderRadius: 10,
    height: 255,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 35,
    width: 300,
    elevation:5
  },
  Content: {
    alignItems: `center`,
    backgroundColor: `#B57BA6`,
    flex: 1,
  }
});