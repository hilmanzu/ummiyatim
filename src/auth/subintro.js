import React, {Component} from 'react';
import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import firebase from 'react-native-firebase';

export default class Intro extends Component{

  render() {
    const { navigate } = this.props.navigation;

    return (
        <View style={styles.introcontainer}>
            <Text style={styles.texthallo}>
              Halo !
            </Text>
            <Text style={styles.textindramayu}>
              Daftar Sebagai
            </Text>
            <View style={{marginTop:40}}>
              <TouchableOpacity style={styles.buttonintro} onPress={()=>navigate('register_donatur')}>
                  <Text style={styles.textbuttonintro}>Donatur</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginTop:20}}>
              <TouchableOpacity style={styles.buttonintro} onPress={()=>navigate('register_yatim')}>
                  <Text style={styles.textbuttonintro}>Ummi Yatim</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginTop:20}}>
              <TouchableOpacity style={styles.buttonintro} onPress={()=>navigate('register_peduli')}>
                  <Text style={styles.textbuttonintro}>Ummi Peduli</Text>
              </TouchableOpacity>
            </View>
        </View>
    );
  }
  signOut = () => {
    firebase.auth().signOut();
  }
}