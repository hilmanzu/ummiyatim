import React, {Component} from 'react';
import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import Intros from '../component/intros'
import firebase from 'react-native-firebase';

export default class Intro extends Component{

  render() {
    const { navigate } = this.props.navigation;

    return (
        <View style={styles.introcontainer}>
            <Text style={styles.texthallo}>
              Halo !
            </Text>
            <Text style={styles.textindramayu}>
              Bersama <Text style={{fontWeight:'bold'}}>Ummi Yatim</Text> Mensejahterahkan ummi dan anaknya jadi lebih mudah
            </Text>
            <View style={{marginTop:40}}>
              <TouchableOpacity style={styles.buttonintro} onPress={()=>navigate('login')}>
                  <Text style={styles.textbuttonintro}>LOGIN</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginTop:20}}>
              <TouchableOpacity style={styles.buttonintro} onPress={()=>navigate('subintro')}>
                  <Text style={styles.textbuttonintro}>DAFTAR</Text>
              </TouchableOpacity>
            </View>
            <Intros/>
        </View>
    );
  }
}