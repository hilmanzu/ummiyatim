import React, { Component,Fragment } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  View,
  Alert,
  ActivityIndicator,
  Modal,
  Button
} from "react-native";
import store from 'react-native-simple-store';
import Loading from '../component/loading';
import firebase from 'react-native-firebase';
import LottieView from 'lottie-react-native'
import DropdownAlert from 'react-native-dropdownalert';

const successImageUri = 'https://cdn.pixabay.com/photo/2015/06/09/16/12/icon-803718_1280.png';

export default class PhoneAuthTest extends Component {
  constructor(props) {
    super(props);
    this.unsubscribe = null;
    this.ref = firebase.firestore().collection('user')
    this.state = {
      user: null,
      message: '',
      codeInput: '',
      phoneNumber: '+62',
      confirmResult: null,
      loading: true,
      view:false
    };
  }

  componentDidMount() {
    const {params} = this.props.navigation.state;
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.props.navigation.navigate('home')
      } else {
        this.setState({
          user: null,
          message: '',
          codeInput: '',
          phoneNumber: '+62',
          confirmResult: params.confirm,
        });
      }
    });
  }

  componentWillUnmount() {
     if (this.unsubscribe) this.unsubscribe();
  }

  confirmCode = () => {
    const { codeInput, confirmResult } = this.state;
    const {params} = this.props.navigation.state;
    if (confirmResult && codeInput.length) {
      confirmResult.confirm(codeInput)
        .then((user) => {
          this.props.navigation.navigate('splash')
        })
        .catch(error =>{
          this.dropdown.alertWithType('error', 'Error', 'Kode konfirmasi Error')
        });
    }
  };

  render() {
    const { codeInput } = this.state;
    return (
      <Fragment>
        <View style={styles.Content}>
          <DropdownAlert ref={ref => this.dropdown = ref}/>
          <LottieView
            source={require('../component/phonever.json')}
            autoPlay
            loop
            style={{width:190,height:190}}
          />
          <View style={styles.card}>
            <Text style={styles.text1}>
              Masukkan Kode Verifikasi
            </Text>
            <TextInput  
              placeholder={'Kode Verifikasi ....'} 
              keyboardType='numeric'
              style={styles.textinput} 
              onChangeText={value => this.setState({ codeInput: value })}
              value={codeInput}
            />
            <TouchableOpacity  onPress={this.confirmCode}>
              <View style={styles.masuk}>
                  <Text style={styles.bottonmasuk}>Verfikasi</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <Modal visible={this.state.view} transparent={true} >
          <Loading/>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  logo: { height: 80, marginLeft: 140, marginRight: 140, width: 80 },
  text1: {
    color: '#B57BA6',
    fontSize: 18,
    fontWeight: `bold`,
    marginLeft: 32,
    marginRight: 95,
    marginTop: 25,
    width:200
  },
  text2: {
    color: `rgba(170, 170, 170, 1)`,
    fontSize: 12,
    marginLeft: 32,
    marginRight: 85,
    marginTop: 15
  },
  textinput1: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 17,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
    width: 250,elevation:5
  },
  textinput: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 17,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
    width: 250,elevation:5
  },
  text3: {
    color: `rgba(34, 25, 77, 0.44)`,
    fontSize: 12,
    marginLeft: 45,
    marginTop: 15
  },
  bottonmasuk: { color: `rgba(255, 255, 255, 1)`, fontWeight: `bold` },
  masuk: {
    alignItems: `center`,
    backgroundColor: '#B57BA6',
    borderRadius: 30,
    height: 37,
    justifyContent: `center`,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 15,
    width: 250,elevation:5
  },
  text4: { color: '#B57BA6', textAlign: `center` },
  buttondaftar: {
    alignItems: `center`,
    justifyContent: `center`,
    marginLeft: 90,
    marginRight: 90,
    marginTop: 23
  },
  card: {
    backgroundColor: `rgba(247, 247, 247, 1)`,
    borderRadius: 10,
    height: 255,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 35,
    width: 300,
    elevation:5
  },
  Content: {
    alignItems: `center`,
    backgroundColor: `#B57BA6`,
    flex: 1,
  }
});