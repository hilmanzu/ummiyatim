import React, {Component} from 'react';
import {Picker,Modal,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../assets/style'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import Loading from '../component/loading'
import firebase from 'react-native-firebase';
import store from 'react-native-simple-store';
import provinsi from '../component/provinsi.json'
import kabupaten from '../component/kabupaten.json'
import kecamatan from '../component/kecamatan.json'
import renderIf from '../assets/renderIf'
import LottieView from 'lottie-react-native'

export default class login extends Component{
  constructor(props){
    super(props);
    this.unsubscribe = null;
    this.ref = firebase.firestore().collection('user')
    this.agama = firebase.firestore().collection('master').doc('data').collection('agama')
    this.state = {
      ///phone auth
      user: null,
      message: '',
      codeInput: '',
      phoneNumber: '+628',
      confirmResult: null,
      ////
      view          : false,
      nik           : '',
      nama          : '',
      tempat_lahir  : '',
      tanggal_lahir : '',
      agama         : '',
      pekerjaan     : '',
      minat         : '',
      provinsi      : '',
      kabupaten     : '',
      kecamatan     : '',
      hp            : '+62',
      pendaftaran   : '',
      urutan        : 0,
      data          : [],
      data_agama    : [],
      }
    }

  componentDidMount(){

      this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          this.ref.add({
              nik           : this.state.nik,
              nama          : this.state.nama,
              tempat_lahir  : this.state.tempat_lahir,
              tanggal_lahir : this.state.tanggal_lahir,
              agama         : this.state.agama,
              pekerjaan     : this.state.pekerjaan,
              minat         : this.state.minat,
              provinsi      : this.state.provinsi,
              kabupaten     : this.state.kabupaten,
              kecamatan     : this.state.kecamatan,
              phone         : this.state.phoneNumber,
              pendaftaran   : 'peduli',
              image         : 'http://cdn.onlinewebfonts.com/svg/img_947.png',
              waktu         : new Date(),
              id_registrasi : this.state.registrasi,
              verifikasi    : false
          })
        } else {
          // User has been signed out, reset the state
          this.setState({
            user: null,
            message: '',
            codeInput: '',
            confirmResult: null,
          });
        }
      });

      this.ref.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          data : data,
          urutan : data.map((data)=> data.length)
        })
      })

      this.agama.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          data_agama : data
        })
      })
  }

  componentWillUnmount() {
     if (this.unsubscribe) this.unsubscribe();
  }

   daftar = () => {
    const {params} = this.props.navigation.state;
    const {data} = this.state
    if (this.state.nik == ''){
      alert('Isi Nik')
    }else if (this.state.nama == ''){
      alert('Isi Nama Lengkap')
    }else if (this.state.tempat_lahir == ''){
      alert('Isi tempat lahir')
    }else if (this.state.tanggal_lahir == ''){
      alert('Isi tanggal lahir')
    }else if (this.state.agama == ''){
      alert('Isi agama')
    }else if (this.state.pekerjaan == ''){
      alert('Isi pekerjaan')
    }else if (this.state.minat == ''){
      alert('Isi minat')
    }else if (this.state.provinsi == ''){
      alert('Isi provinsi')
    }else if (this.state.kabupaten == ''){
      alert('Isi kabupaten')
    }else if (this.state.kecamatan == ''){
      alert('Isi kecamatan')
    }else if (this.state.phoneNumber == ''){
      alert('Isi No Hp')
    }else{
      var Id = data.find((item) => item.data.phone === this.state.phoneNumber)
      if (Id === undefined){
        firebase.auth().signInWithPhoneNumber(this.state.phoneNumber)
        .then(confirmResult =>{
          this.setState({ confirmResult })
          this.ref.add({
              nik           : this.state.nik,
              nama          : this.state.nama,
              tempat_lahir  : this.state.tempat_lahir,
              tanggal_lahir : this.state.tanggal_lahir,
              agama         : this.state.agama,
              pekerjaan     : this.state.pekerjaan,
              minat         : this.state.minat,
              provinsi      : this.state.provinsi,
              kabupaten     : this.state.kabupaten,
              kecamatan     : this.state.kecamatan,
              phone         : this.state.phoneNumber,
              pendaftaran   : 'peduli',
              image         : 'http://cdn.onlinewebfonts.com/svg/img_947.png',
              waktu         : new Date(),
              id_registrasi : data.length,
              verifikasi    : false
            })
        })
        .catch(error =>{
          Alert.alert('Hai..','Masuk dengan nomor Ponsel gagal, Silahkan coba lagi.')
        });
      }else if (Id.data.phone === this.state.phoneNumber){
        Alert.alert('Hai..','Nomor ini sudah terdaftar silahkan login ya.')
      }
    }
  }

  confirmCode = () => {
    const { codeInput, confirmResult } = this.state;
    const {data} = this.state

    if (confirmResult && codeInput.length) {
      confirmResult.confirm(codeInput)
        .then((user) => {
          this.ref.add({
              nik           : this.state.nik,
              nama          : this.state.nama,
              tempat_lahir  : this.state.tempat_lahir,
              tanggal_lahir : this.state.tanggal_lahir,
              agama         : this.state.agama,
              pekerjaan     : this.state.pekerjaan,
              minat         : this.state.minat,
              provinsi      : this.state.provinsi,
              kabupaten     : this.state.kabupaten,
              kecamatan     : this.state.kecamatan,
              phone         : this.state.phoneNumber,
              pendaftaran   : 'peduli',
              waktu         : new Date(),
              image         : 'http://cdn.onlinewebfonts.com/svg/img_947.png',
              id_registrasi : data.length,
              verifikasi    : false
            })
          .then((d)=>{
            Alert.alert('Selamat','anda sudah terdaftar')
            this.props.navigation.navigate('splash')
          })
        })
        .catch(error => alert(error.message));
    }
  };

  renderPhoneNumberInput(){
    const { data_agama } = this.state
    const kab = kabupaten.filter((item) => item.province_id === this.state.provinsi)
    const kec = kecamatan.filter((item) => item.regency_id === this.state.kabupaten)

    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
          <Text style={styles.headertext}>  Daftar Ummi Peduli</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false}>

          <Text style={styles.textpassword}>
            NIK
          </Text>
          <View style={styles.textinput}>
              <TextInput keyboardType='numeric' onChangeText={(nik)=> this.setState({nik})}/>
          </View>

          <Text style={styles.textpassword}>
            Nama Lengkap
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(nama)=> this.setState({nama})}/>
          </View>

          <Text style={styles.textpassword}>
            Tempat Lahir
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(tempat_lahir)=> this.setState({tempat_lahir})}/>
          </View>

          <Text style={styles.textpassword}>
            Tanggal Lahir
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(tanggal_lahir)=> this.setState({tanggal_lahir})}/>
          </View>

          <Text style={styles.textpassword}>
            Agama
          </Text>
          <View style={styles.itempekerjaan}>
            <Picker
              selectedValue={this.state.agama}
              onValueChange={(agama) => this.setState({agama})}>
              <Picker.Item label='Pilih Agama' value={''} />
              {data_agama.map((item, index) => {
              return (<Picker.Item label={item.data.nama} value={item.data.nama} key={item.id}/>) 
              })}
            </Picker>
          </View>

          <Text style={styles.textpassword}>
            Pekerjaan
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(pekerjaan)=> this.setState({pekerjaan})}/>
          </View>

          <Text style={styles.textpassword}>
            Minat
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(minat)=> this.setState({minat})}/>
          </View>

          <Text style={styles.textpassword}>
            Provinsi
          </Text>
          <View style={styles.itempekerjaan}>
            <Picker
              selectedValue={this.state.provinsi}
              onValueChange={(itemValue, provinsi) => this.setState({provinsi:itemValue,id:itemValue})}>
              <Picker.Item label='Pilih Provinsi' value={''} />
              {provinsi.map((item, index) => {
              return (<Picker.Item label={item.name} value={item.id} key={item.id}/>) 
              })}
            </Picker>
          </View>

          <Text style={styles.textpassword}>
            Kabupaten
          </Text>
          <View style={styles.itempekerjaan}>
            <Picker
              selectedValue={this.state.kabupaten}
              onValueChange={(itemValue, kabupaten) => this.setState({kabupaten:itemValue,id:itemValue})}>
              <Picker.Item label='Pilih Kabupaten/kota' value={''}/>
              {kab.map((item, index) => {
              return (<Picker.Item label={item.name} value={item.id} key={item.id}/>) 
              })}
            </Picker>
          </View>

          <Text style={styles.textpassword}>
            Kecamatan
          </Text>
          <View style={styles.itempekerjaan}>
            <Picker
              selectedValue={this.state.kecamatan}
              onValueChange={(kecamatan) => this.setState({kecamatan})}>
              <Picker.Item label='Pilih Kecamatan' value={''}/>
              {kec.map((item, index) => {
              return (<Picker.Item label={item.name} value={item.name} key={item.id}/>) 
              })}
            </Picker>
          </View>

          <Text style={styles.textfirst}>
            Nomor HP
          </Text>
          <View style={styles.textinput}>
            <TextInput keyboardType='numeric' onChangeText={(phoneNumber)=> this.setState({phoneNumber})} value={this.state.phoneNumber}/>
          </View>

          <View style={{alignItems:'center',marginVertical:20}}>
            <TouchableOpacity style={styles.button} disabled={this.state.button} onPress={this.daftar}>
              <Text style={styles.textbutton}>Buat Akun</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        </View>
        <Modal visible={this.state.view} transparent={true} >
          <Loading/>
        </Modal>
      </View>
    );
  }

  renderVerificationCodeInput() {
    const { codeInput } = this.state;
    return (
        <View style={styles.Contents}>
          <LottieView
            source={require('../component/phonever.json')}
            autoPlay
            loop
            style={{width:190,height:190}}
          />
          <View style={styles.cards}>
            <Text style={styles.text1s}>
              Masukkan Kode Verifikasi
            </Text>
            <TextInput  
              placeholder={'Kode Verifikasi ....'} 
              keyboardType='numeric'
              style={styles.textinputs} 
              onChangeText={value => this.setState({ codeInput: value })}
              value={codeInput}
            />
            <TouchableOpacity  onPress={this.confirmCode}>
              <View style={styles.masuks}>
                  <Text style={styles.bottonmasuks}>Verfikasi</Text>
              </View>
            </TouchableOpacity>
          </View>
        <Modal visible={this.state.view} transparent={true} >
          <Loading/>
        </Modal>
      </View>
    );
  }

  render() {
    const { user, confirmResult } = this.state;
    return (
      <View style={{ flex: 1 }}>

        {!user && !confirmResult && this.renderPhoneNumberInput()}

        {!user && confirmResult && this.renderVerificationCodeInput()}

      </View>
    );
  }

}