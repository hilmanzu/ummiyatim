import React from 'react';
import {BackHandler,Alert,View, Text,  StyleSheet, Image ,PermissionsAndroid,Platform,ActivityIndicator} from 'react-native';
import store from 'react-native-simple-store';
import Loading from './component/loading'
import firebase from "react-native-firebase"

export default class Splashscreen extends React.Component {

  constructor(props){
    super(props);
    console.ignoredYellowBox = ['Setting a timer']
  }


  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
    this.props.navigation.navigate(user ? 'home' : 'intro')
    console.log(user)
      const items = firebase.firestore().collection("user")
      items.onSnapshot(async(doc)=>{
        var data = []
          doc.forEach((docs)=>{
            let item = data
            item.push({
              data : docs.data(),
              id   : docs.id
            })
          })
        const profile = data.find((item) => item.data.phone === user.phoneNumber)
        const profileId = profile.id
        store.save('id',profileId)
      })
    })
  }

  render() {
    return (
      <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#F5FCFF'}}>
        <Loading/>
      </View>
    )
  }
}

const styles = StyleSheet.create ({
   container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  image: {
    width: 80,
    height: 80,
    marginBottom:20
  }
})