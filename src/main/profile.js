import React, {Component} from 'react';
import {BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import store from 'react-native-simple-store';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import firebase from "react-native-firebase"
var numeral = require('numeral');
import Loading from '../component/loading'
import renderIf from '../component/renderIf'

export default class profile extends Component{

  static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Profile',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={{uri:'https://i.ibb.co/KjB5XYb/002-man-user.png'}}
          style={[{width:20,height:20}, { tintColor: tintColor }]}
        />
      ),
  };

  constructor(props) {
    super(props)
      this.state = {
        loading : true,
        nama    : '',
        image   : '',
        anak    : [],
        data    : '',
      }
  }

  componentDidMount(){

  firebase.auth().onAuthStateChanged(user => {
    const {params} = this.props.navigation.state;
    const db = firebase.firestore()
    const item = db.collection("user")

      item.onSnapshot(async(doc)=>{
        var data = []
          doc.forEach((docs)=>{
            let item = data
            item.push({
              data : docs.data(),
              id   : docs.id
            })
          })
        const profile = data.find((item) => item.data.phone === user.phoneNumber)
        const profileId = profile.id
        const namaid = profile.data.nama
        const pendaftaran = profile.data.pendaftaran
        const items = db.collection("user").doc(profileId)
        const anak = db.collection("user").doc(profileId).collection("tanggungan")
        store.save('id',profileId)
        store.save('nama',namaid)
        store.save('pendaftaran',pendaftaran)

        items.onSnapshot( async (doc) => {
          let data = doc.data()
          this.setState({ data:data })
        })

        anak.onSnapshot(async(doc)=>{
          var data = []
            doc.forEach((docs)=>{
              let item = data
              item.push({
                data : docs.data(),
                id   : docs.id
              })
            })
          this.setState({
            anak : data,loading:false
          })
        })

      })
    })

  }


  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    const { loading,anak,data } =this.state

    if (loading === true) {
        return(
          <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
            <Loading/>
          </View>
        )
      }

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={{width:30,height:30,marginLeft:20}} source={require('../assets/logo.png')}/>
          <Text style={styles.headertext}>Ummi Profile</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow:1}}>
            <Image style={{width:Dimensions.get('window').width,height:200}} source={{uri : data.image}}/>
            <Text style={{marginTop:10,marginBottom:10,fontSize:20,fontWeight:'bold',marginLeft:20}}>{data.nama}</Text>
            <View>
              {renderIf(data.verifikasi === true ? true : false)(
                <Text style={{marginBottom:10,fontSize:15,fontWeight:'bold',marginLeft:20}}>Sudah Terverifikasi</Text>
              )}
              {renderIf(data.verifikasi !== true ? true : false)(
                <Text style={{marginBottom:10,fontSize:15,fontWeight:'bold',marginLeft:20}}>Belum Terverifikasi</Text>
              )}
            </View>
            <View style={{flexDirection:'row'}}>
              <View style={{flex:1,marginLeft:20}}>
                <Text>Nama</Text>
                <Text>Tempat Lahir</Text>
                {renderIf(data.pendaftaran === 'yatim' ? true : false)(
                  <Text>Status</Text>
                )}
                <Text>Agama</Text>
                {renderIf(data.pendaftaran === 'yatim' ? true : false)(
                  <Text>Tanggungan</Text>
                )}
                <Text>Pekerjaan</Text>
                <Text>Kecamatan</Text>
                <Text>Minat</Text>
                {renderIf(data.minat_rt === true && data.pendaftaran === 'yatim' ? true : false)(
                  <Text>Ingin Menikah</Text>
                )}
                {renderIf(data.pendaftaran === 'yatim' ? true : false)(
                  <Text>Perekonomian</Text>
                )}
              </View>
              <View style={{flex:2}}>
                <Text>: {data.nama}</Text>
                <Text>: {data.tempat_lahir}/ {data.tanggal_lahir}</Text>
                {renderIf(data.pendaftaran === 'yatim' ? true : false)(
                  <Text>: {data.status}</Text>
                )}
                <Text>: {data.agama}</Text>
                {renderIf(data.pendaftaran === 'yatim' ? true : false)(
                  <Text>: {data.tanggungan}</Text>
                )}
                <Text>: {data.pekerjaan}</Text>
                <Text>: {data.kecamatan}</Text>
                <Text>: {data.minat}</Text>
                {renderIf(data.minat_rt === true && data.pendaftaran === 'yatim' ? true : false)(
                  <Text>: Berminat</Text>
                )}
                {renderIf(data.minat_rt !== true && data.pendaftaran === 'yatim' ? true : false)(
                  <Text>: Tidak Berminat</Text>
                )}
                {renderIf(data.pendaftaran === 'yatim' ? true : false)(
                  <Text>: {data.kondisi_eko}</Text>
                )}
              </View>
            </View>
            {renderIf(data.pendaftaran === 'yatim' ? true : false)(
              <Text style={{marginTop:10,marginBottom:10,fontSize:20,fontWeight:'bold',marginLeft:20}}>Tanggungan</Text>
            )}
            <View style={{alignItems:'center',justifyContent:'center'}}>
              {anak.map((data)=>
                <View style={{marginVertical:10,marginHorizontal:2}}>
                  <View style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} >
                    <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:14,fontWeight:'bold',marginLeft:25,color:'#149BE5'}}>{data.data.nama}</Text>
                    <View style={{flexDirection:'row',marginLeft:5,alignItems:'center',marginTop:5}}>
                      <View style={{flex:1,marginLeft:20}}>
                        <Text>Nama</Text>
                        <Text>Tempat Lahir</Text>
                        <Text>Cita - cita</Text>
                      </View>
                      <View style={{flex:2}}>
                        <Text>: {data.data.nama}</Text>
                        <Text>: {data.data.tempat_lahir}/ {data.data.tanggal_lahir}</Text>
                        <Text>: {data.data.cita}</Text>
                      </View>
                    </View>
                  </View>
                </View>
                )
              }
            </View>
            {renderIf(data.pendaftaran === 'yatim' ? true : false)(
              <View style={{alignItems:'center',marginVertical:20}}>
                <TouchableOpacity style={styles.button} onPress={this.tambahkan}>
                  <Text style={styles.textbutton}>Tambahkan Tanggungan</Text>
                </TouchableOpacity>
              </View>
            )}
            <View style={{alignItems:'center',marginVertical:20}}>
              <TouchableOpacity style={styles.button} onPress={this.keluar}>
                <Text style={styles.textbutton}>Keluar</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>

      </View>
    );
  }

  keluar=()=>{
    firebase.auth().signOut();
    store.delete('id')
    store.delete('nama')
    this.props.navigation.navigate('splash')
  }

  tambahkan=()=>{
    this.props.navigation.navigate('tanggungan')
  }
  
}
