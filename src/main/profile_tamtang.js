import React, {Component} from 'react';
import {Picker,Modal,ScrollView,Text, View,Image,ImageBackground,TextInput,TouchableOpacity,Alert} from 'react-native';
import styles from '../assets/style'
import Loading from '../component/loading'
import firebase from 'react-native-firebase';
import store from 'react-native-simple-store';

export default class login extends Component{
  constructor(props){
    super(props);
    this.state = {
      data          : [],
      view          : false,
      nik           : '',
      nama          : '',
      tempat_lahir  : '',
      tanggal_lahir : '',
      sekolah       : '',
      cita          : '',
      prestasi      : '',
      minat         : '',
      }
    }

  render() {

    return (
      <View style={styles.subcontainer}>
        <View style={styles.header}>
          <Text style={styles.headertext}>  Data Tanggungan</Text>
        </View>
        <View style={styles.const}>
        <ScrollView showsVerticalScrollIndicator={false}>

          <Text style={styles.textpassword}>
            NIK
          </Text>
          <View style={styles.textinput}>
              <TextInput keyboardType='numeric' onChangeText={(nik)=> this.setState({nik})}/>
          </View>

          <Text style={styles.textpassword}>
            Nama Lengkap
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(nama)=> this.setState({nama})}/>
          </View>

          <Text style={styles.textpassword}>
            Tempat Lahir
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(tempat_lahir)=> this.setState({tempat_lahir})}/>
          </View>

          <Text style={styles.textpassword}>
            Tanggal Lahir
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(tanggal_lahir)=> this.setState({tanggal_lahir})}/>
          </View>

          <Text style={styles.textpassword}>
            Sekolah
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(sekolah)=> this.setState({sekolah})}/>
          </View>

          <Text style={styles.textpassword}>
            Hobi
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(minat)=> this.setState({minat})}/>
          </View>

          <Text style={styles.textpassword}>
            Cita-cita
          </Text>
          <View style={styles.textinput}>
              <TextInput onChangeText={(cita)=> this.setState({cita})}/>
          </View>

          <Text style={styles.textpassword}>
            Prestasi
          </Text>
          <View style={styles.textinput}>
              <TextInput placeholder={'contoh : 1.a , 2.b'} onChangeText={(prestasi)=> this.setState({prestasi})}/>
          </View>

          <View style={{alignItems:'center',marginVertical:20}}>
            <TouchableOpacity style={styles.button} disabled={this.state.button} onPress={this.daftar}>
              <Text style={styles.textbutton}>Tambahkan Tanggungan</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        </View>
        <Modal visible={this.state.view} transparent={true} >
          <Loading/>
        </Modal>
      </View>
    );
  }

  daftar=()=>{

    store.get('id').then((res)=>{

      const push = firebase.firestore().collection('user').doc(res).collection('tanggungan')
      push.add({
          nik           : this.state.nik,
          nama          : this.state.nama,
          tempat_lahir  : this.state.tempat_lahir,
          tanggal_lahir : this.state.tanggal_lahir,
          hobi         : this.state.minat,
          sekolah       : this.state.sekolah,
          cita          : this.state.cita,
          prestasi      : this.state.prestasi,
          image         : 'https://i.ibb.co/0QDKgsN/boy.png'
        })
      .then(result => {
        Alert.alert('Berhasil','berhasil tambah tanggungan')
        this.props.navigation.goBack()
      })
      .catch(error => {
        console.log('Transaction failed:', error);
      });

    })
  }

}