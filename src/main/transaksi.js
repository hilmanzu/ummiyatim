import React, {Component} from 'react';
import {BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import store from 'react-native-simple-store';
import firebase from "react-native-firebase"
import renderIf from "../assets/renderIf"
var numeral = require('numeral');

export default class home extends Component{

  static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Transaksi',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={{uri:'https://i.ibb.co/hFCG6B7/004-buy.png'}}
          style={[{width:20,height:20}, { tintColor: tintColor }]}
        />
      ),
    };

  constructor(props) {
    super(props)
      this.banner = firebase.firestore().collection("transaksi").orderBy('waktu', 'asc')
      this.state = {
        loading: true,
        data:[],
        Donatur:'',
        pendaftaran:'',
        iduser:'',
        namauser:'',
        pendaftaran:''
      }
  }

  componentDidMount(){
    const db = firebase.firestore()
    const item = db.collection("user")
    firebase.auth().onAuthStateChanged(user => {

      item.onSnapshot(async(doc)=>{
          var data = []
            doc.forEach((docs)=>{
              let item = data
              item.push({
                data : docs.data(),
                id   : docs.id,
              })
            })
          const profile = data.find((item) => item.data.phone === user.phoneNumber)
          const profileId = profile.id
          const namaid = profile.data.nama
          const pendaftaran = profile.data.pendaftaran
          this.setState({
            iduser:profileId,
            namauser:namaid,
            pendaftaran:pendaftaran
          })
      })

    })

    store.get('id')
    .then((res)=>{
      this.banner.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          data : data,
          Donatur : res
        })
      })
    })

  }

  render() {
    const { navigate } = this.props.navigation;
    const { data,merek,arrival,pendaftaran,iduser } = this.state

    const filter_donatur = data.filter((data)=> data.data.id_donatur === iduser )
    const filter_ummi = data.filter((data)=> data.data.id_penerima === iduser )

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={{width:30,height:30,marginLeft:20}} source={require('../assets/logo.png')}/>
          <Text style={styles.headertext}>Transaksi</Text>
        </View>
          {renderIf(pendaftaran === 'donatur' ? true : false)(
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{alignItems:'center'}}>
              {filter_donatur.map((data)=>
                <View style={{marginVertical:10,marginHorizontal:2}}>
                  <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} >
                    {renderIf(data.data.nama_transaksi === 'Donasi' ? true : false)(
                      <Image source={require('../assets/donation.png')} style={{position:'absolute',marginLeft:10,width:100,height:100,marginTop:10}}/>
                    )}
                    {renderIf(data.data.nama_transaksi === 'Taaruf' ? true : false)(
                      <Image source={require('../assets/taaruf.png')} style={{position:'absolute',marginLeft:10,width:100,height:100,marginTop:10}}/>
                    )}
                    <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:18,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.data.nama_transaksi}</Text>
                    <Text style={{width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#000000'}}>Kepada</Text>
                    <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                      <Text numberOfLines={1} style={{width:150}}>{data.data.Penerima}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                      <Text>Status</Text>
                      <Text numberOfLines={1} style={{width:150}}>:  {data.data.status}</Text>
                    </View>
                  </TouchableOpacity>
                </View>
                )
              }
            </ScrollView>
          )}
          {renderIf(pendaftaran === 'yatim' ? true : false)(
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{alignItems:'center'}}>
              {filter_ummi.map((data)=>
                <View style={{marginVertical:10,marginHorizontal:2}}>
                  <TouchableOpacity style={{width:320,height:120,backgroundColor:'#fff',borderRadius:5,elevation:5}} >
                    {renderIf(data.data.nama_transaksi === 'Donasi' ? true : false)(
                      <Image source={require('../assets/donation.png')} style={{position:'absolute',marginLeft:10,width:100,height:100,marginTop:10}}/>
                    )}
                    {renderIf(data.data.nama_transaksi === 'Taaruf' ? true : false)(
                      <Image source={require('../assets/taaruf.png')} style={{position:'absolute',marginLeft:10,width:100,height:100,marginTop:10}}/>
                    )}
                    <Text numberOfLines={2} style={{marginTop:10,width:180,fontSize:18,fontWeight:'bold',marginLeft:140,color:'#149BE5'}}>{data.data.nama_transaksi}</Text>
                    <Text style={{width:180,fontSize:14,fontWeight:'bold',marginLeft:140,color:'#000000'}}>Kepada</Text>
                    <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                      <Text numberOfLines={1} style={{width:150}}>{data.data.Penerima}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginLeft:140,alignItems:'center',marginTop:5}}>
                      <Text>Status</Text>
                      <Text numberOfLines={1} style={{width:150}}>:  {data.data.status}</Text>
                    </View>
                  </TouchableOpacity>
                </View>
                )
              }
            </ScrollView>
          )}
      </View>
    );
  }
}
