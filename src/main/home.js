import React, {Component} from 'react';
import {BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import store from 'react-native-simple-store';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import firebase from "react-native-firebase"
var numeral = require('numeral');


const { width: viewport } = Dimensions.get('window')
const sliderWidth1 = wp(100)
const itemHorizontalMargin1 = wp(0.30)
const sliderWidth = viewport
const itemWidth = (sliderWidth1 + (itemHorizontalMargin1 * 2)) - 10

function wp(percentage){
  const value = (percentage * viewport) / 100
  return Math.round(value)
}

export default class home extends Component{

  static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Home',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={{uri:'https://i.ibb.co/Vxm6NxJ/003-tablet.png'}}
          style={[{width:20,height:20}, { tintColor: tintColor }]}
        />
      ),
    };

  constructor(props) {
    super(props)
      this.banner = firebase.firestore().collection("master").doc("data").collection("banner")
      this.merek = firebase.firestore().collection("user").doc("oAiLWCGAqpVUK30TuWIp").collection("merek_hp")
      this.arrival = firebase.firestore().collection("master").doc("oAiLWCGAqpVUK30TuWIp").collection("all_data")
      this.state = {
        dataSource: [],
        loading: true,
        jadwal:'',
        data:[],
        merek:[],
        arrival:[]
      }
  }

  componentDidMount(){
    store.get('uid')
    .then((res)=>{
      this.banner.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          data : data
        })
      })

      this.merek.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          merek : data
        })
      })

      this.arrival.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          arrival : data.filter((item) => item.data.new_arrival === true)
        })
      })

    })
  }

   _renderItem ({item, index}) {
        return (
            <View>
                <Image source={{uri:item.data.image}} style={{width:Dimensions.get('window').width,height:220,borderBottomRightRadius:100}}/>
            </View>
        );
    }

    get pagination () {
        const { data, activeSlide } = this.state;
        return (
            <Pagination
              dotsLength={data.length}
              activeDotIndex={activeSlide}
              containerStyle={{borderRadius: 5,width:20,height:5,position:'absolute',marginTop:180,marginLeft:315}}
              dotStyle={{width: 10,height: 10,borderRadius: 5,backgroundColor: '#B57BA6'}}
              inactiveDotStyle={{backgroundColor: 'rgba(0, 0, 0, 0.75)'}}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
            />
        );
    }


  render() {
    const { navigate } = this.props.navigation;
    const { data,merek,arrival } = this.state

    return (
      <View style={styles.container}>
        <View style={{position:'absolute',backgroundColor:'#B57BA6',width:Dimensions.get('window').width,height:220,borderBottomRightRadius:100}}>
        </View>
        <View style={styles.header}>
          <Image style={{width:30,height:30,marginLeft:20}} source={require('../assets/logo.png')}/>
          <Text style={styles.headertext}>Ummi Yatim</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{alignItems:'center'}}>
            <View style={{flex:1}}>
              <Carousel
                ref={(c) => { this._carousel = c; }}
                data={data}
                autoplay = {true}
                loop = {true}
                renderItem={this._renderItem}
                onSnapToItem={(index) => this.setState({ activeSlide: index }) }
                sliderWidth={Dimensions.get('window').width}
                itemWidth={Dimensions.get('window').width}
                sliderHeight={180}
                itemHeight={180}
              />
              {this.pagination}
            </View>
            <View style={{flexDirection:'row',marginTop:20}}>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('listdonation')}>
                <Image source={require('../assets/donation.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Ummi Donasi</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('listtaaruf')}>
                <Image source={require('../assets/taaruf.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Ummi Taaruf</Text>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row',marginTop:3,marginBottom:20}}>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('JenisPekerjaan')}>
                <Image source={require('../assets/strategy.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Ummi Kreatif</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.layanancategory} onPress={() => this.props.navigation.navigate('Pariwisata')}>
                <Image source={require('../assets/volunteer.png')} style={styles.imagecategory}/>
                <Text style={styles.textcategory}>Ummi Peduli</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
      </View>
    );
  }
  keluar=()=>{
    this.props.navigation.navigate('Splash')
    store.delete('token')
  }
}
