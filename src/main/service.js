import React, {Component} from 'react';
import {BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../assets/style'
import store from 'react-native-simple-store';
import firebase from "react-native-firebase"
var numeral = require('numeral');

export default class home extends Component{

  static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Transaksi',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={{uri:'https://i.ibb.co/hFCG6B7/004-buy.png'}}
          style={[{width:20,height:20}, { tintColor: tintColor }]}
        />
      ),
    };

  constructor(props) {
    super(props)
      this.banner = firebase.firestore().collection("transaksi")
      this.state = {
        loading: true,
        data:[],
      }
  }

  componentDidMount(){
    store.get('uid')
    .then((res)=>{
      this.banner.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          data : data
        })
      })
    })
  }

  render() {
    const { navigate } = this.props.navigation;
    const { data,merek,arrival } = this.state

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={{width:30,height:30,marginLeft:20}} source={require('../assets/logo.png')}/>
          <Text style={styles.headertext}>Ummi Yatim</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{alignItems:'center'}}>
          </ScrollView>
      </View>
    );
  }
}
