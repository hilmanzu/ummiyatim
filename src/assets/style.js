import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';


const styles = StyleSheet.create({
  textt:{
    fontSize:15
  },
  textt:{
    fontSize:15
  },
  header:{
    width:Dimensions.get('window').width,
    height:50,
    backgroundColor:'#B57BA6',
    flexDirection:'row',
    alignItems:'center'
  },
  headertext:{
    color:'#fff',
    marginLeft:20,
    fontWeight:'bold'
  },
  container: {
    flex: 1,
    alignItems:'center',
    backgroundColor:'#fff',
  },
  introcontainer: {
    flex: 1,
    alignItems:'center',
    backgroundColor:'#B57BA6'
  },
  subcontainer: {
    flex: 1,
    backgroundColor:'#fff'
  },
  ImageBackground:{
    width: '100%',
    height: '100%',
    flex:1,
  },
  ImageLogo:{
    height:170,position:'absolute',width: Dimensions.get('window').width,
  },
  ImageLogo2:{
    height:230,position:'absolute',width: Dimensions.get('window').width,
  },
  texthallo:{
    color:'#fff',
    textAlign:'center',
    marginTop: 109,
    marginHorizontal:35,
    fontSize:14
  },
  textindramayu:{
    color:'#fff',
    textAlign:'center',
    marginHorizontal:35,
    fontSize:14
  },
  button:{
    borderRadius:30,
    backgroundColor:'#B57BA6',
    height:52,
    width:250,
    justifyContent:'center',
    elevation:5
  },
  textbutton:{
    color:'#FFFFFF',
    fontSize: 20,
    textAlign:'center',
    fontWeight:'bold'
  },
  buttonintro:{
    borderRadius:30,
    backgroundColor:'#fff',
    height:52,
    width:250,
    justifyContent:'center',
    elevation:5
  },
  textbuttonintro:{
    color:'#B57BA6',
    fontSize: 20,
    textAlign:'center',
    fontWeight:'bold'
  },
  oval:{
    position:'absolute',
    alignItems:'center',
    justifyContent:'center',
    height:126,
    width:126,
    borderRadius:100,
    marginTop:67,
    backgroundColor:'#F2FBFF',
    elevation:5
  },
  square:{
    height:420,
    width:320,
    borderRadius:10,
    marginTop:130,
    backgroundColor:'#fff',
    elevation:5
  },
  textphone:{
    fontSize:14,
    marginTop:96,
    marginLeft:20
  },
  textinput:{
    backgroundColor:'#F2FBFF',
    borderColor: '#B57BA6',
    borderRadius:10,
    borderWidth: 1,
    height:40,
    width:280,
    marginHorizontal:20,
    marginTop:15
  },
  item:{
    backgroundColor:'#F2FBFF',
    borderColor: '#B57BA6',
    borderRadius:10,
    borderWidth: 1,
    height:40,
    width:280,
    marginHorizontal:20,
    marginTop:15,
    justifyContent:'center'
  },
  itempekerjaan:{
    backgroundColor:'#F2FBFF',
    borderColor: '#B57BA6',
    borderRadius:10,
    borderWidth: 1,
    height:40,
    width:280,
    marginTop:15,
    marginHorizontal:20,
    justifyContent:'center'
  },
  textinput2:{
    backgroundColor:'#F2FBFF',
    borderColor: '#B57BA6',
    borderRadius:10,
    borderWidth: 1,
    height:280,
    width:280,
    marginHorizontal:20,
    marginTop:15
  },
  textpassword:{
    fontSize:14,
    marginTop:15,
    marginLeft:20
  },
  textsandi:{
    fontSize:11,
    marginTop:20,
    marginLeft:214,
    color:'#007CBF'
  },
  const:{
    alignItems:'center',
    flex:1
  },
  textfirst:{
    fontSize:14,
    marginTop:20,
    marginLeft:20
  },
  layananbutton:{
    borderRadius:20,
    backgroundColor:'#FFFFFF',
    height:60,
    marginBottom:20,
    elevation: 5,
    flexDirection:'row',
    alignItems:'center'
  },
  layananbuttonimage:{
    width:340,
    height:50,
    marginBottom:20,
    marginTop:20
  },
  layananbuttontext:{
    color:'#FD0000',
    fontSize: 16,
    fontWeight:'bold',
    marginLeft:20,
    flex:1
  },
  imagelayanan:{
    width:24,
    height:24,
    marginRight:20
  },
  layanancategory:{
    marginTop:4,
    backgroundColor:'#FFFFFF',
    alignItems:'center',
    justifyContent:'center',
    paddingTop:9,
    paddingBottom:15,
    paddingHorizontal:30,
    marginHorizontal:8,
    borderRadius:5,
    elevation: 10,
  },
  layanancategorytext:{
    color:'#FD0000',
    fontSize: 16,
    fontWeight:'bold'
  },
  imagecategory:{
    width:47,
    height:47,
  },
  textcategory:{
    fontSize:15,
    marginTop:7
  },
  //pbar dan pengaduan
  pbar:{
    alignItems:'center',
    marginTop:35,
    backgroundColor:'#fff',
    borderRadius:10,
    elevation:5
  },
  textpbar:{
    alignItems:'center',
    borderLeftColor:'#000000',
    borderRightColor:'#000000',
    borderTopColor:'#fff',
    borderBottomColor:'#fff'
  },
  textpbar2:{
    alignItems:'center',
    marginHorizontal:20,
  },
  texttotal:{
    color:'#29A8ED',
    fontSize:20
  },
  texttotal2:{
    color:'#000000',
    fontSize:15
  },
  texttotal3:{
    color:'#000000',
    fontSize:10,
  },
  pbard:{
    height :96,
    width  :320,
    borderRadius:10,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
  },
  //layanan
  boxlayanan:{
    width:320,
    height:144,
    backgroundColor:'#B57BA6',
    marginTop:20,
    borderRadius:10
  },
  boxlayanan2:{
    width:100,
    height:45,
    backgroundColor:'#fff',
    marginTop:90,
    marginLeft:25,
    borderTopLeftRadius:20,
    borderBottomLeftRadius:20,
    justifyContent:'center',
    flexDirection:'row',
    alignItems:'center',
    position:'absolute',
    elevation:5
  },
  boxlayanan3:{
    width:100,
    height:45,
    backgroundColor:'#fff',
    marginTop:90,
    marginLeft:128,
    borderTopRightRadius:20,
    borderBottomRightRadius:20,
    justifyContent:'center',
    flexDirection:'row',
    alignItems:'center',
    position:'absolute',
    elevation:5
  },
  box:{
    width:40,
    height:40,
    backgroundColor:'#fff',
    borderRadius:20,
    justifyContent:'center',
    flexDirection:'row',
    alignItems:'center',
    position:'absolute',
    elevation:5
  },
  box2:{
    width:90,
    height:30,
    backgroundColor:'#fff',
    marginLeft:95,
    borderTopRightRadius:20,
    borderBottomRightRadius:20,
    justifyContent:'center',
    flexDirection:'row',
    alignItems:'center',
    position:'absolute',
    elevation:2
  },
  textbl1:{
    fontSize:16,
    color:'#fff',
    position:'absolute',
    marginTop:25,
    marginLeft:25
  },
  textbl2:{
    fontSize:25,
    color:'#fff',
    position:'absolute',
    marginTop:45,
    marginLeft:25,
    fontWeight:'bold',
    width:200,
  },
  imagebl:{
    position:'absolute',
    marginTop:25,
    marginLeft:230,
    width:67,
    height:67,
  },
  imagebl2:{
    width:20,
    height:20,
    tintColor:'#FF1919'
  },
  imagebl3:{
    width:20,
    height:20,
    tintColor:'#000'
  },
  ////confirmcode
  logos: { height: 80, marginLeft: 140, marginRight: 140, width: 80 },
  text1s: {
    color: '#B57BA6',
    fontSize: 18,
    fontWeight: `bold`,
    marginLeft: 32,
    marginRight: 95,
    marginTop: 25,
    width:200
  },
  text2s: {
    color: `rgba(170, 170, 170, 1)`,
    fontSize: 12,
    marginLeft: 32,
    marginRight: 85,
    marginTop: 15
  },
  textinput1s: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 17,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
    width: 250,elevation:5
  },
  textinputs: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 17,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
    width: 250,elevation:5
  },
  text3s: {
    color: `rgba(34, 25, 77, 0.44)`,
    fontSize: 12,
    marginLeft: 45,
    marginTop: 15
  },
  bottonmasuks: { color: `rgba(255, 255, 255, 1)`, fontWeight: `bold` },
  masuks: {
    alignItems: `center`,
    backgroundColor: '#B57BA6',
    borderRadius: 30,
    height: 37,
    justifyContent: `center`,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 15,
    width: 250,elevation:5
  },
  text4s: { color: '#B57BA6', textAlign: `center` },
  buttondaftars: {
    alignItems: `center`,
    justifyContent: `center`,
    marginLeft: 90,
    marginRight: 90,
    marginTop: 23
  },
  cards: {
    backgroundColor: `rgba(247, 247, 247, 1)`,
    borderRadius: 10,
    height: 255,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 35,
    width: 300,
    elevation:5
  },
  Contents: {
    alignItems: `center`,
    backgroundColor: `#B57BA6`,
    flex: 1,
  }

})

export default styles